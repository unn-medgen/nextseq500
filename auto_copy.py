import os, shutil, csv, random
from datetime import datetime
import logging
from logging.handlers import RotatingFileHandler

# Source folder should be D:\Output. Target folders should be defined on tos-miseqprep
# <year>\TrueSight One_<date> needs to be parsed/created
source_folder = r"D:\Output"
medgen_folder = r'Z:\Illumina\Medisinsk genetikk\Analyser NextSeq 500'
molpat_folder = r'Z:\Illumina'

# Logging config.
log = r"nextseq_autotools.log"
logcomplete = r"nextseq_autotools.complete.log"

# Set the maximum file size to 1 MB
max_file_size = 1 * 1024 * 1024  # 50 MB in bytes

# Create a logger
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

# complete writes to nextseq_autotools.complete.log which contains all events
complete_handler = RotatingFileHandler(filename=logcomplete, maxBytes=max_file_size, backupCount=1, encoding='utf-8', delay=False)
complete_handler.setLevel(logging.INFO)  # Set the logging level to INFO
complete_formatter = logging.Formatter('%(asctime)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
complete_handler.setFormatter(complete_formatter)
complete_handler.propagate = False

# log writes to nextseq_autotools.log which contains only successful events
log_handler = RotatingFileHandler(filename=log, maxBytes=max_file_size, backupCount=1, encoding='utf-8', delay=False)
log_handler.setLevel(logging.WARNING)  # Set the logging level to WARNING
log_formatter = logging.Formatter('%(asctime)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
log_handler.setFormatter(log_formatter)
log_handler.propagate = False

# Add handlers
logger.addHandler(complete_handler)
logger.addHandler(log_handler)

# Look through D:\Output. If any folders have "RTAComplete.txt" and "CopyComplete.txt", copy to tos-miseqprep
def copy_folders(source_folder, medgen_folder, molpat_folder):
    for root, dirs, files in os.walk(source_folder):
        # Skip source folder
        if root == source_folder:
            continue
        # Skip if iterating through any subfolders of runfolders
        rel_path = os.path.relpath(root, source_folder)
        if rel_path != '.' and rel_path.count(os.path.sep) >= 1:
            continue
        # Check if "RTAComplete.txt" and "CopyComplete.txt" exists. If they do, that means sequencing for this run is complete
        if "RTAComplete.txt" in files and "CopyComplete.txt" in files:
            # Parse SampleSheet.csv to get "Experiment Name" and "year" for medgen_folder path
            csv_file_path = os.path.join(root, "SampleSheet.csv")
            if os.path.isfile(csv_file_path):
                with open(csv_file_path, 'r') as csv_file:
                    csv_reader = csv.reader(csv_file)
                    lines = list(csv_reader)

                    if lines[2][0].strip() == "Experiment Name" and lines[3][0].strip() == "Date":
                        experiment_name = lines[2][1].strip()
                        date = lines[2][1].strip().split('_')[1]
                        date = datetime.strptime(date, "%y%m%d")
                        year = date.year
                        date_en = date.strftime("%y%m%d")
                        date_no = date.strftime("%d%m%y")

                        # Experiment Name "TSOexp" is used by MedGen for legacy reasons 
                        if experiment_name.startswith("TSOexp"):
                            # Create parent folder from info in SampleSheet.csv
                            medgen_folder = os.path.join(medgen_folder, str(year))
                            target_folder = "TruSight One exp_{}".format(str(date_en))
                            medgen_folder = os.path.join(medgen_folder, target_folder)

                            # Check if parent target folder exists and create
                            if not os.path.exists(medgen_folder):
                                os.makedirs(medgen_folder)

                            # Create absolute target folder and copy
                            folder_to_copy = os.path.abspath(root)
                            destination_path = os.path.join(medgen_folder, os.path.relpath(folder_to_copy, source_folder))
                            try:
                                print ("Kopierer {0} til {1}".format(folder_to_copy, destination_path))
                                print ("Dette tar typisk 3-4 timer.")
                                print ("Husk å sjekke nextseq_autotools.log etterpå!")
                                shutil.copytree(folder_to_copy, destination_path)
                                logger.warning("OK (MedGen) - {0} - Kopiert til {1}".format(folder_to_copy, destination_path))
                            except Exception as e:
                                logger.info("Warning (MedGen) - {0} - Ble ikke kopiert. %s".format(root), e, exc_info=False)

                        # Experiment Name "MOLPAT" is used by Molecular Pathology for any runs
                        elif experiment_name.startswith("MOLPAT"):
                            logger.info("Warning (MolPat) - {0} - Gjenkjente MOLPAT sekvensering, men regler for flytting er ikke implementert enda. Overfør manuelt og/eller kontakt bioinformatiker".format(root))

                        # If no matching "Experiment Name" is found, do nothing and write to log
                        else:
                            logger.info("Warning - {0} - Sekvenseringen er ferdig, men Experiment Name er udefinert eller det finnes ingen regler for dette kjøret. Overfør manuelt og/eller kontakt bioinformatiker".format(root))
                    else:
                        logger.info("Warning - {0} - SampleSheet.csv ble lest, men ikke gjenkjent. Har formatet endret seg?".format(root))
            else:
                logger.info("Warning - {0} - SampleSheet.csv ble ikke funnet".format(root))
        else:
            logger.info("Warning - {0} - er ikke ferdigsekvensert. (RTAComplete.txt og CopyComplete.txt ikke funnet)".format(root))

# For testing - Dev. Remove these when deploying to production
#source_folder = r"/Users/emr023/UNN/Nextseq-outputfolder-taskmanager/testfolder"
#medgen_folder = r"/Users/emr023/UNN/Nextseq-outputfolder-taskmanager/targetfolder"
#molpat_folder = r"/Users/emr023/UNN/Nextseq-outputfolder-taskmanager/targetfolder2"

# Call the function to copy folders
copy_folders(source_folder, medgen_folder, molpat_folder)
