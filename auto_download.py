import os, shutil, csv, random, subprocess
from datetime import datetime
import logging
from logging.handlers import RotatingFileHandler

# Source folder should be D:\Output. Target folders should be defined on tos-miseqprep
# <year>\TrueSight One_<date> needs to be parsed/created
source_folder = r"D:\Output"
medgen_folder = r'Z:\Illumina\Medisinsk genetikk\Analyser NextSeq 500'

# Logging config. Should probably be r'Z:\nextseq_autotools\nextseq_autotools.log' where the script also lives
log = r"nextseq_autotools.log"
logcomplete = r"nextseq_autotools.complete.log"

# Set the maximum file size to 1 MB
max_file_size = 1 * 1024 * 1024  # 50 MB in bytes

# Create a logger
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

# complete writes to nextseq_autotools.complete.log which contains all events
complete_handler = RotatingFileHandler(filename=logcomplete, maxBytes=max_file_size, backupCount=1, encoding='utf-8', delay=False)
complete_handler.setLevel(logging.INFO)  # Set the logging level to INFO
complete_formatter = logging.Formatter('%(asctime)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
complete_handler.setFormatter(complete_formatter)
complete_handler.propagate = False

# log writes to nextseq_autotools.log which contains only successful events
log_handler = RotatingFileHandler(filename=log, maxBytes=max_file_size, backupCount=1, encoding='utf-8', delay=False)
log_handler.setLevel(logging.WARNING)  # Set the logging level to WARNING
log_formatter = logging.Formatter('%(asctime)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
log_handler.setFormatter(log_formatter)
log_handler.propagate = False

# Add handlers
logger.addHandler(complete_handler)
logger.addHandler(log_handler)

### BS settings
# Before using this script, make sure to auth using "bs auth -c eu --api-server https://api.euc1.sh.basespace.illumina.com"
# For linux, './bs' - For windows '.\bs.exe'
#application_path = './bs'
application_path = r'.\bs.exe'

# Auth
def check_auth(application_path):
    print("Autentiserer med Basespace...")
    try:
        # Run bs.exe auth to check whether account is set up properly
        whoami = ['whoami', '-f', 'csv']
        output = subprocess.run([application_path] + whoami, check=True, capture_output=True, text=True)
        csv_reader = csv.reader(output.stdout.split('\n'), delimiter=',')
        lines = list(csv_reader)
        # Bjørn Nygård euc1 id
        if lines[1][1] == '2596598':
            print("OK")
        else:
            print ("Autentisering mislykket / Feil bruker")
            logger.info('Warning - Autentisering mislykket / Feil bruker. Prøv å kjør "bs auth -c default --api-server https://api.euc1.sh.basespace.illumina.com" på nytt')
            exit()

    except subprocess.CalledProcessError as e:
        print(f"Autentisering mislykket: {e}")
        logger.info("Warning - Autentisering mislykket: %s", e, exc_info=False)

# List projects
def list_projects(application_path):
    print("Henter prosjekter fra basespace...")
    try:
        # Run the external application
        listprojects = ['list', 'projects', '-f', 'csv']
        output = subprocess.run([application_path] + listprojects, check=True, capture_output=True, text=True)
        csv_reader = csv.reader(output.stdout.split('\n'), delimiter=',')
        lines = list(csv_reader)
        lines.pop(0)
        print("OK")
        return lines
    except subprocess.CalledProcessError as e:
        print(f"Ingen prosjekter funnet i Basespace: {e}")
        logger.info("Warning - Ingen prosjekter funnet i Basespace: %s", e, exc_info=False)

# List runs
def list_runs(application_path):
    print("Henter runs fra basespace...")
    try:
        # Run the external application
        listprojects = ['list', 'runs', '-f', 'csv']
        output = subprocess.run([application_path] + listprojects, check=True, capture_output=True, text=True)
        csv_reader = csv.reader(output.stdout.split('\n'), delimiter=',')
        lines = list(csv_reader)
        lines.pop(0)
        print("OK")
        return lines
    except subprocess.CalledProcessError as e:
        print(f"Ingen runs funnet i Basespace: {e}")
        logger.info("Warning - Ingen runs funnet i Basespace: %s", e, exc_info=False)

# Look through D:\Output. If any folders have "RTAComplete.txt" and "CopyComplete.txt", copy to tos-miseqprep
def download(source_folder, medgen_folder):
    for root, dirs, files in os.walk(source_folder):
        # Skip source folder
        if root == source_folder:
            continue
        # Skip if iterating through any subfolders of runfolders
        rel_path = os.path.relpath(root, source_folder)
        if rel_path != '.' and rel_path.count(os.path.sep) >= 1:
            continue
        # Check if "RTAComplete.txt" and "CopyComplete.txt" exists. If they do, that means sequencing for this run is complete
        if "RTAComplete.txt" in files and "CopyComplete.txt" in files:
            # Parse SampleSheet.csv to get "Experiment Name" for bs -n
            csv_file_path = os.path.join(root, "SampleSheet.csv")
            if os.path.isfile(csv_file_path):
                with open(csv_file_path, 'r') as csv_file:
                    csv_reader = csv.reader(csv_file)
                    lines = list(csv_reader)

                    if lines[2][0].strip() == "Experiment Name" and lines[3][0].strip() == "Date":
                        experiment_name = lines[2][1].strip()
                        date = lines[2][1].strip().split('_')[1]
                        date = datetime.strptime(date, "%y%m%d")
                        year = date.year
                        date_en = date.strftime("%y%m%d")
                        date_no = date.strftime("%d%m%y")

                        # Experiment Name "TSOexp" is used by MedGen for legacy reasons 
                        if experiment_name.startswith("TSOexp"):
                            # Check that project exists in Basespace. Get id.
                            projects = list_projects(application_path)
                            projects = {str(item[0]): item[1] for item in projects if item}
                            if not experiment_name in projects.keys():
                                logger.info("Warning (MedGen) - {0} - ikke funnet under prosjekter i Basespace.".format(experiment_name))
                                exit("{0} - ikke funnet under prosjekter i Basespace.".format(experiment_name))
                            else:
                                # If project exists, check that run exists
                                runid = projects[experiment_name]
                                runs = list_runs(application_path)
                                runs = {str(item[2]): item[3] for item in runs if item}
                                if not experiment_name in runs.keys():
                                    logger.info("Warning (MedGen) - {0} - ikke funnet under runs i Basespace.".format(experiment_name))
                                    exit("{0} - ikke funnet under runs i Basespace.".format(experiment_name))
                                else:
                                    # Check that run is complete
                                    print("Sjekker om {} er ferdig prosessert...".format(experiment_name))
                                    if not runs[experiment_name] == 'Complete':
                                        logger.info("Warning (MedGen) - {0} - er ikke ferdig prosessert i Basespace".format(experiment_name))
                                        exit("{0} - er ikke ferdig prosessert i Basespace.".format(experiment_name))
                                    else:
                                        print ("OK")

                            # Create absolute path for folder on nas based on info in SampleSheet.csv and runid from basespace project
                            medgen_folder = os.path.join(medgen_folder, str(year))
                            target_folder = "TruSight One exp_{}".format(str(date_en))
                            medgen_folder = os.path.join(medgen_folder, target_folder)
                            medgen_folder = os.path.join(medgen_folder, experiment_name+"-"+runid)

                            # Check if parent target folder exists and create
                            if not os.path.exists(medgen_folder):
                                os.makedirs(medgen_folder)

                            # Source path
                            folder_to_download = os.path.abspath(root)

                            try:
                                #runid = '24389389'
                                download = ['download', 'project', '-i', str(runid), '-o', medgen_folder, '--summary']
                                print ("Laster ned {0} fra basespace til {1}".format(experiment_name, medgen_folder))
                                print ("Dette tar typisk 3-4 timer.")
                                print ("Husk å sjekke nextseq_autotools.log etterpå!")
                                subprocess.run([application_path] + download, check=True)
                                logger.warning("OK (MedGen) - {0} ({1}) - Lastet ned og overført til tos-miseqrep".format(experiment_name, folder_to_download))
                            except Exception as e:
                                logger.info("Warning (MedGen) - {0} ({1}) - Ble ikke lastet ned. %s".format(experiment_name, folder_to_download), e, exc_info=False)

                        # If no matching "Experiment Name" is found, do nothing and write to log
                        else:
                            logger.info("Warning - {0} - Sekvenseringen er ferdig, men Experiment Name er udefinert eller det finnes ingen regler for dette kjøret. Last opp manuelt og/eller kontakt bioinformatiker".format(root))
                    else:
                        logger.info("Warning - {0} - SampleSheet.csv ble lest, men ikke gjenkjent. Har formatet endret seg?".format(root))
            else:
                logger.info("Warning - {0} - SampleSheet.csv ble ikke funnet".format(root))
        else:
            logger.info("Warning - {0} - er ikke ferdigsekvensert. (RTAComplete.txt og CopyComplete.txt ikke funnet)".format(root))

# For testing - Dev. Remove these when deploying to production
#source_folder = r"/Users/emr023/UNN/Nextseq-outputfolder-taskmanager/testfolder"
#medgen_folder = r"/Users/emr023/UNN/Nextseq-outputfolder-taskmanager/targetfolder"

# Perform an auth check
check_auth(application_path)
# Upload run to Basespace
download(source_folder, medgen_folder)
